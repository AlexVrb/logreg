#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "boost/tokenizer.hpp"

#include "LogisticRegression.h"

// temporary hardcoded; probably should remove later
const unsigned int numOfClasses = 9;

int inputFromFile(const std::string& fileName,
                  matrix<double>& objects,
                  std::vector<int>& labels)
{
    std::ifstream file(fileName, std::ifstream::in);

    if(!file)
    {
        std::cerr << "Can't open file " + fileName + "!" << '\n';
        return -1;
    }

    std::string line = "";
    while(getline(file, line))
    {
        std::vector<double> object;

        //создадим токенайзер, разделящий строку на значения
        boost::tokenizer<boost::escaped_list_separator<char> > tk(
            line,
            boost::escaped_list_separator<char>('\\', ',', '\"'));

        //и пройдёмся по полученной коллекции
        for(boost::tokenizer<boost::escaped_list_separator<char> >::iterator it(tk.begin()); it != tk.end(); ++it)
        {
            std::string temp = *it;
            double number = 0;

            if(!temp.empty())
            {
              number = std::stod(temp);
              object.push_back(number);
            }
        }
        objects.push_back(object);
    }

    file.close();

    for(unsigned int i = 0; i < objects.size(); ++i)
    {
      labels.push_back(objects[i][objects[i].size() - 1]);
      objects[i].pop_back();
    }

    return 0;
}

int main(int argc, char** argv)
{
  if(argc < 4)
  {
    std::cerr << "Not enough args.\nEnter number of threads, precision and learning rate.\n";
    return -1;
  }

  unsigned int numOfThreads = std::stoi(argv[1]);
  double precision = std::stod(argv[2]);
  double rate = std::stod(argv[3]);

  matrix<double> trainObjects;
  std::vector<int> labels;
  int result = inputFromFile("objectMatrix.txt", trainObjects, labels);
  if(result)
  {
    std::cerr << "Failed to read training file" << '\n';
    return result;
  }

  double smoothing = 1 / static_cast<double>(numOfClasses);

  std::cout << "Read file\n";

  // вызвать обучение логит-регрессии
  LogisticRegression logreg(trainObjects[0].size(), numOfClasses, numOfThreads);

  std::cout << "Logit-regression initialized\n";

  result = logreg.train(trainObjects, labels, precision, rate, smoothing);
  if(result)
  {
    std::cerr << "Error while training: " << result << '\n';
  }

  std::cout << "Training finished\n";

  return 0;
}
