#ifndef CommonDefines_H
#define CommonDefines_H

#include <vector>

template <typename T>
using matrix = std::vector<std::vector<T> >;

#endif
