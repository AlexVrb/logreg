#include <cfloat>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>

#include "cblas.h"

#include "LogisticRegression.h"

const unsigned int c_maxOfIterations = pow(10, 5);

LogisticRegression::LogisticRegression(const unsigned int numOfFeatures,
                                       const unsigned int numOfClasses,
                                       const unsigned int numOfThreads) :
  m_numOfFeatures(numOfFeatures),
  m_numOfClasses(numOfClasses),
  m_numOfThreads(numOfThreads)
{
  std::cout << "Using " << numOfFeatures << " features, " << numOfClasses << " classes and " << numOfThreads << " threads.\n";

  m_classifierWeights.resize(m_numOfClasses);
  for(unsigned int i = 0; i < m_classifierWeights.size(); ++i)
  {
    m_classifierWeights[i].resize(m_numOfFeatures + 1);
  }

  // установим OpenBLAS в однопоточный режим, т.к. его многопоточность может конфликтовать с нашей
  openblas_set_num_threads(1);
}

// функция обучения
int LogisticRegression::train(const matrix<double>& objects,
                              const std::vector<int>& labels,
                              const double& precision,
                              const double& rate,
                              const double& smoothing)
{
  std::cout << std::setprecision(6) << "Precision: " << precision <<
                                       "\nRate: " << rate <<
                                       "\nSmoothing: " << smoothing << '\n';

  // засечем время начала
  auto begin = std::chrono::high_resolution_clock::now();

  for(unsigned int i = 0; i < m_classifierWeights.size(); ++i)
  {
    for(unsigned int j = 0; j < m_classifierWeights[i].size(); ++j)
    {
      m_classifierWeights[i][j] = 0;
    }
  }

  matrix<double> predictions;
  int result = predict(objects, predictions);

  double score = logloss(predictions, labels);

  // инициализация генератора случайных чисел с равномерным распределением на отрезке, содержащем номера всех объектов в матрице
  // в качестве сида используем местное время
  std::default_random_engine rnd(std::chrono::high_resolution_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<unsigned int> dist(0, objects.size() - 1);

  unsigned int numOfIterations = 0;

  // условия выхода - либо достижение заданной точности, либо превышение количества итераций,
  // т.к. оптимизация может не сойтись
  while(score >= precision && numOfIterations < c_maxOfIterations)
  {
    ++numOfIterations;

    // выбор объектов
    matrix<double> selectedObjects(m_numOfThreads);
    std::vector<int> selectedLabels(m_numOfThreads);
    for(unsigned int i = 0; i < selectedObjects.size(); ++i)
    {
      unsigned int randomIndex = dist(rnd);
      selectedObjects[i] = objects[randomIndex];
      selectedLabels[i] = labels[randomIndex];
    }

    std::vector<std::thread> threads(m_numOfThreads);
    matrix<double> gradients(m_numOfThreads);

    std::vector<double> errors(m_numOfThreads);

    // запуск потоков
    for(unsigned int i = 0; i < threads.size(); ++i)
    {
      threads[i] = std::thread(&LogisticRegression::calculateOneObject,
                               this,
                               std::ref(selectedObjects[i]),
                               selectedLabels[i],
                               std::ref(gradients[i]),
                               std::ref(errors[i]));
    }
    //синхронизация потоков
    for(unsigned int i = 0; i < threads.size(); ++i)
    {
      threads[i].join();
    }

    // выполнение шага оптимизации
    for(unsigned int i = 0; i < gradients.size(); ++i)
    {
      cblas_daxpy(gradients[i].size(),
                  rate,
                  gradients[i].data(),
                  1,
                  m_classifierWeights[selectedLabels[i] - 1].data(),
                  1);
    }

    double sum = 0;
    for(unsigned int i = 0; i < errors.size(); ++i)
    {
      sum += errors[i];
    }
    sum = sum / errors.size();

    score = (1 - smoothing) * score + smoothing * sum;
    //std::cout << "Current score: " << std::setprecision(6) << score << '\n';
  }

  // засечем время окончания
  auto end = std::chrono::high_resolution_clock::now();

  if(numOfIterations >= c_maxOfIterations)
  {
    std::cerr << "Optimization didn't converge!\n";
  }

  unsigned int duration = std::chrono::duration_cast<std::chrono::seconds>(end - begin).count();

  std::cout << "Training ended in "
            << numOfIterations
            << " iterations and "
            << duration
            << " seconds with result "
            << std::setprecision(6)
            << score
            << '\n';

  return 0;
}

// функция, вычисляемая каждым потоком
void LogisticRegression::calculateOneObject(const std::vector<double>& object,
                                            const int label,
                                            std::vector<double>& gradient,
                                            double& error)
{
  const matrix<double> oneobj(1, object);
  const std::vector<int> onelbl(1, label);
  matrix<double> prediction;

  int result = predict(oneobj, prediction);
  if(prediction.size() != 1)
  {
    std::cerr <<
      "There are more than 1 predictions! " <<
      prediction.size() <<
      '\n';
  }

  error = logloss(prediction, onelbl);

  gradient = calculateGradient(object, label);
}

// функция вычисления принадлежности объектов классам
int LogisticRegression::predict(const matrix<double>& objects,
                                matrix<double>& predictions,
                                matrix<double> weights) const
{
  if(weights.size() == 0)
  {
    //default
    weights = m_classifierWeights;
  }

  predictions.resize(objects.size());

  for(unsigned int i = 0; i < predictions.size(); ++i)
  {
    predictions[i].resize(m_numOfClasses);

    for(unsigned int j = 0; j < predictions[i].size(); ++j)
    {
      double classificationResult = cblas_ddot(m_numOfFeatures,
                                               objects[i].data(),
                                               1,
                                               weights[j].data(),
                                               1);
      predictions[i][j] = 1 / (exp(- classificationResult) + 1);
    }
  }

  return 0;
}

// функция ошибки
double LogisticRegression::logloss(const matrix<double>& predictions,
                                   const std::vector<int>& labels) const
{
  double sum = 0;
  for(unsigned int i = 0; i < predictions.size(); ++i)
  {
    for(unsigned int j = 0; j < m_numOfClasses; ++j)
    {
      int belongs = labels[i] == j ? 1 : 0;
      sum += belongs * log(predictions[i][j]);
    }
  }

  return - sum / predictions.size();
}

// вычисление градиента
std::vector<double> LogisticRegression::calculateGradient(const std::vector<double>& vec,
                                                          const int label) const
{
  std::vector<double> partialDerivatives(vec.size());

  const matrix<double> oneobj(1, vec);
  const std::vector<int> onelbl(1, label);

  double step = sqrt(DBL_EPSILON);
  for(unsigned int i = 0; i < partialDerivatives.size(); ++i)
  {
    matrix<double> tempCopy1 = m_classifierWeights;

    tempCopy1[label - 1][i] += step;
    matrix<double> pred1, pred2;
    int tempRes1 = predict(oneobj, pred1, tempCopy1);
    double score1 = logloss(pred1, onelbl);

    matrix<double> tempCopy2 = m_classifierWeights;
    tempCopy2[label - 1][i] -= step;
    int tempRes2 = predict(oneobj, pred2, tempCopy2);
    double score2 = logloss(pred2, onelbl);

    partialDerivatives[i] = (score2 - score1) / (2 * step);
  }

  return partialDerivatives;
}
