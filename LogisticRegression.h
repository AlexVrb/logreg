#ifndef LogisticRegression_H
#define LogisticRegression_H

#include <mutex>
#include <string>
#include <thread>

#include "CommonDefines.h"

class LogisticRegression
{
  public:
    LogisticRegression(const unsigned int numOfFeatures,
                       const unsigned int numOfClasses,
                       const unsigned int numOfThreads = std::thread::hardware_concurrency());

    int train(const matrix<double>& objects,
              const std::vector<int>& labels,
              const double& precision,
              const double& rate,
              const double& smoothing);

    int predict(const matrix<double>& objects,
                matrix<double>& predictions,
                matrix<double> weights = matrix<double>(0)) const;

  protected:
    double logloss(const matrix<double>& predictions,
                   const std::vector<int>& labels) const;

    void calculateOneObject(const std::vector<double>& object,
                            const int label,
                            std::vector<double>& gradient,
                            double& error);

    std::vector<double> calculateGradient(const std::vector<double>& vec,
                                          const int label) const;

    matrix<double> m_classifierWeights;
    unsigned int m_numOfFeatures;
    unsigned int m_numOfClasses;

    unsigned int m_numOfThreads;

    std::mutex m_crit;
};
#endif
